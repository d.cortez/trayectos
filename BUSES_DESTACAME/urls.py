from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include('BusesAPI.urls')),
    path('',include('UsuariosAPI.urls')),
    path('',include('TrayectoAPI.urls')),
]
