from django.db import models
from UsuariosAPI.models import Chofer, Pasajero
from BusesAPI.models import Bus

class Trayecto(models.Model):
    recorrido = (('SANTIAGO','SANTIAGO'),('VIÑA DEL MAR','VIÑA DEL MAR'),('VALDIVIA','VALDIVIA'))

    id_trayecto = models.IntegerField(default=0000,primary_key=True)
    bus = models.ForeignKey(Bus,on_delete=models.CASCADE,blank=False, null=False)
    chofer = models.ForeignKey(Chofer,on_delete=models.CASCADE,blank=False, null=False)
    origen = models.CharField(max_length=30,choices=recorrido)
    destino = models.CharField(max_length=30,choices=recorrido)
    estado = models.CharField(max_length=30, default='PENDIENTE')
    pasajeros = models.ManyToManyField(Pasajero)

    class Meta:
        unique_together = ["bus", "chofer", "origen","destino","estado"]


"""
Listar a los trayectos junto a su promedio de pasajeros.
Filtrar a todos los buses de un trayecto con más del N % de su capacidad vendida.
"""