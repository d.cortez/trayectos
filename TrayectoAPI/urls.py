from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'trayecto',views.TrayectoView)

urlpatterns = [
	path('trayecto/',include(router.urls))
]
