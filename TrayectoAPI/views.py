from django.shortcuts import render
from rest_framework import viewsets
from .models import Trayecto
from .serializers import TrayectoSerializer
from django.db.models import Count

# Create your views here.
class TrayectoView(viewsets.ModelViewSet):
	queryset = Trayecto.objects.all()
	serializer_class = TrayectoSerializer
