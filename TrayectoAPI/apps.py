from django.apps import AppConfig


class TrayectoapiConfig(AppConfig):
    name = 'TrayectoAPI'
