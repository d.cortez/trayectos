from django import forms
from .models import Trayecto


class TrayectoForm(forms.ModelForm):
    class Meta:
        model = Trayecto
        fields = ('__all__')