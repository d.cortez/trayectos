from django.db import models

# Create your models here.
class Bus(models.Model):
    id_bus = models.IntegerField(default=0000, primary_key = True)
    serie_bus = models.CharField(max_length=40)

    def __str__(self):
        return str(self.id_bus)
