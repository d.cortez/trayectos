from django.apps import AppConfig


class BusesapiConfig(AppConfig):
    name = 'BusesAPI'
