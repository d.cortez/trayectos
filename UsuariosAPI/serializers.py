from rest_framework import serializers, status
from .models import Chofer,Pasajero

class ChoferSerializer(serializers.ModelSerializer):
	class Meta:
		model = Chofer
		fields= ('__all__')

class PasajeroSerializer(serializers.ModelSerializer):
	class Meta:
		model = Pasajero
		fields= ('__all__')


 
