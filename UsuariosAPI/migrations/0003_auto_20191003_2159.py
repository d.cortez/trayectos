# Generated by Django 2.2.5 on 2019-10-03 19:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('UsuariosAPI', '0002_auto_20191001_2304'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='chofer',
            name='usuario',
        ),
        migrations.RemoveField(
            model_name='pasajero',
            name='usuario',
        ),
    ]
