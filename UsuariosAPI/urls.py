from django.urls import path,include
from .views import PasajeroView,ChoferView
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'pasajero',PasajeroView,basename='pasajero')
router.register(r'chofer',ChoferView,basename='chofer')

urlpatterns = [
	path('usuarios/',include(router.urls)),
]
