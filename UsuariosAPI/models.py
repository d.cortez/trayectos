from django.db import models

class Pasajero(models.Model):
    rut_pasajero = models.CharField(max_length=20,null=False,primary_key = True)
    nombre_pasajero = models.CharField(max_length=20,default="nnn",null=False)
    apellido_pasajero = models.CharField(max_length=20,default="nnn",null=False)
    telefono_pasajero = models.CharField(max_length=15,default="nnn",null=False)

    def __str__(self):
        return self.rut_pasajero

class Chofer(models.Model):
    rut_chofer = models.CharField(max_length=20,null=False,primary_key = True)
    nombre_chofer = models.CharField(max_length=20,default="nnn",null=False)
    apellido_chofer = models.CharField(max_length=20,default="nnn",null=False)
    fecha_contrato = models.DateField(null=False)
   
    def __str__(self):
        return self.rut_chofer


