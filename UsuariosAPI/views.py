from django.shortcuts import render
from rest_framework import viewsets,status
from .models import Chofer,Pasajero
from .serializers import PasajeroSerializer,ChoferSerializer

# Create your views here.
class ChoferView(viewsets.ModelViewSet):
	queryset = Chofer.objects.all()
	serializer_class = ChoferSerializer

class PasajeroView(viewsets.ModelViewSet):
	queryset = Pasajero.objects.all()
	serializer_class = PasajeroSerializer
