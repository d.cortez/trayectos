import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import ListBus from '@/components/Buses/ListBus.vue'
import EditarBus from '@/components/Buses/EditarBus.vue'
import EliminarBus from '@/components/Buses/EliminarBus.vue'
import NuevoBus from '@/components/Buses/NuevoBus.vue'
import ListTrayecto from '@/components/Trayecto/ListTrayecto.vue'
import EditarTrayecto from '@/components/Trayecto/EditarTrayecto.vue'
import EliminarTrayecto from '@/components/Trayecto/EliminarTrayecto.vue'
import NuevoTrayecto from '@/components/Trayecto/NuevoTrayecto.vue'
import NuevoChofer from '@/components/Chofer/NuevoChofer.vue'
import ListChofer from '@/components/Chofer/ListChofer.vue'
import EditarChofer from '@/components/Chofer/EditarChofer.vue'
import EliminarChofer from '@/components/Chofer/EliminarChofer.vue'
import NuevoPasajero from '@/components/Pasajero/NuevoPasajero.vue'
import ListPasajero from '@/components/Pasajero/ListPasajero.vue'
import EditarPasajero from '@/components/Pasajero/EditarPasajero.vue'
import EliminarPasajero from '@/components/Pasajero/EliminarPasajero.vue'
import Index from '@/components/Index/Index.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '',
      name: 'Index',
      component: Index
    },
    {
      path: '/bus',
      name: 'ListBus',
      component: ListBus
    },
    {
      path: '/bus/:id_bus/editar',
      name: 'EditarBus',
      component: EditarBus
    },
    {
      path: '/bus/:id_bus/eliminar',
      name: 'EliminarBus',
      component: EliminarBus
    },
    {
      path: '/bus/nuevo',
      name: 'NuevoBus',
      component: NuevoBus
    },
    {
      path: '/trayecto',
      name: 'ListTrayecto',
      component: ListTrayecto
    },
    {
      path: '/trayecto/:id_trayecto/editar',
      name: 'EditarTrayecto',
      component: EditarTrayecto
    },
    {
      path: '/trayecto/:id_trayecto/eliminar',
      name: 'EliminarTrayecto',
      component: EliminarTrayecto
    },
    {
      path: '/trayecto/nuevo',
      name: 'NuevoTrayecto',
      component: NuevoTrayecto
    },
    {
      path: '/chofer',
      name: 'ListChofer',
      component: ListChofer
    },
    {
      path: '/chofer/:id_chofer/editar',
      name: 'EditarChofer',
      component: EditarChofer
    },
    {
      path: '/chofer/:id_chofer/eliminar',
      name: 'EliminarChofer',
      component: EliminarChofer
    },
    {
      path: '/chofer/nuevo',
      name: 'NuevoChofer',
      component: NuevoChofer
    },
    {
      path: '/pasajero',
      name: 'ListPasajero',
      component: ListPasajero
    },
    {
      path: '/pasajero/:id_pasajero/editar',
      name: 'EditarPasajero',
      component: EditarPasajero
    },
    {
      path: '/pasajero/:id_pasajero/eliminar',
      name: 'EliminarPasajero',
      component: EliminarPasajero
    },
    {
      path: '/pasajero/nuevo',
      name: 'NuevoPasajero',
      component: NuevoPasajero
    }
  ],
  mode:'history'
})
